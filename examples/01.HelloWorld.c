/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <cirrlicht/cirrlicht.h>
#include <stddef.h>

int main()
{
  // create irrlicht window
  irr_core_dimension2d_u32 windowSize = {640, 480};
  irr_IrrlichtDevice *device =
    irr_createDevice(irr_video_EDT_SOFTWARE, &windowSize, 16,
                     false, false, false);

  if (!device)
    {
      return 1;
    }

  // set window caption
  irr_setWindowCaption(device, "Hello World! - Irrlicht Engine Demo");

  // instances for doing things
  irr_video_IVideoDriver* driver = irr_getVideoDriver(device);
  irr_scene_ISceneManager* smgr = irr_getSceneManager(device);
  irr_gui_IGUIEnvironment* guienv = irr_getGUIEnvironment(device);

  // add a static text
  irr_core_rect_s32 box = {10, 10, 260, 22};
  irr_gui_addStaticText(guienv,
                        "Hello World! This is the Irrlicht Software renderer!",
                        &box, true, true, NULL, -1, false);

  // load a quake2 mesh
  irr_scene_IAnimatedMesh* mesh = irr_scene_getMesh(smgr, "media/sydney.md2");
  if (!mesh)
    {
      irr_drop(device);
      return 1;
    }
  irr_scene_IAnimatedMeshSceneNode* node =
    irr_scene_addAnimatedMeshSceneNode(smgr, mesh, NULL, -1,
                                       NULL, NULL, NULL, false);

  if (node)
    {
      irr_scene_setMaterialFlag(node, irr_video_EMF_LIGHTING, false);
      irr_scene_setMD2Animation(node, irr_scene_EMAT_STAND);
      irr_video_ITexture* texture = irr_video_getTexture(driver, "media/sydney.bmp");
      irr_scene_setMaterialTexture(node, 0, texture);
    }

  // camera
  irr_core_vector3df position = {0, 30, -40};
  irr_core_vector3df lookat = {0, 5, 0};
  irr_scene_addCameraSceneNode(smgr, NULL, &position, &lookat, -1, true);
                                             
  // loop
  irr_video_SColor bgcolor = MAKE_COLOR(255, 100, 101, 140);
  while (irr_run(device))
    {
      irr_video_beginScene(driver, true, true, &bgcolor, NULL, NULL);
      irr_gui_drawAll(guienv);
      irr_scene_drawAll(smgr);
      irr_video_endScene(driver);
    }

  irr_drop(device);
  return 0;
}
