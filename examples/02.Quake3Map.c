/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <cirrlicht/cirrlicht.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  // ask user for driver
  irr_video_E_DRIVER_TYPE driverType;

  printf("Please select the driver you want for this example:\n"        \
         " (a) OpenGL 1.5\n (b) Direct3D 9.0c\n (c) Direct3D 8.1\n"     \
         " (d) Burning's Software Renderer\n (e) Software Renderer\n"   \
         " (f) NullDevice\n (otherKey) exit\n\n");

  char i = getchar();

  switch(i)
    {
    case 'a': driverType = irr_video_EDT_OPENGL; break;
    case 'b': driverType = irr_video_EDT_DIRECT3D9;break;
    case 'c': driverType = irr_video_EDT_DIRECT3D8;break;
    case 'd': driverType = irr_video_EDT_BURNINGSVIDEO;break;
    case 'e': driverType = irr_video_EDT_SOFTWARE; break;
    case 'f': driverType = irr_video_EDT_NULL;     break;
    default: return 1;
    }

  // create device and exit if creation failed
  irr_core_dimension2d_u32 windowSize = {640, 480};
  irr_IrrlichtDevice *device =
    irr_createDevice(driverType, &windowSize, 16, false, false, false);

  if (!device)
    {
      return 1;  // could not create selected driver
    }

  // instances for doing things
  irr_video_IVideoDriver* driver = irr_getVideoDriver(device);
  irr_scene_ISceneManager* smgr = irr_getSceneManager(device);

  // load Quake3 map
  irr_io_addFileArchive(irr_getFileSystem(device), "media/map-20kdm2.pk3",
                        true, true, irr_io_EFAT_UNKNOWN, "", NULL);

  irr_scene_IAnimatedMesh* mesh = irr_scene_getMesh(smgr, "20kdm2.bsp");
  irr_scene_ISceneNode* node;

  if (mesh)
    node = irr_scene_addOctreeSceneNode(smgr, mesh, NULL, -1, 512, false);

  if (node)
    {
      irr_core_vector3df newpos = {-1300, -144, -1249};
      irr_scene_setPosition(node, &newpos);
    }

  // FPS camera
  irr_scene_addCameraSceneNodeFPS(smgr, NULL, 100, 0.5, -1, NULL, 0, false, 0, false, true);
  irr_gui_setVisibleCursor(irr_getCursorControl(device), false);
                                             
  // loop
  int lastFPS = -1;
  irr_video_SColor bgcolor = MAKE_COLOR(255, 200, 200, 200);
  while (irr_run(device))
    {
      if (irr_isWindowActive(device))
        {
          irr_video_beginScene(driver, true, true, &bgcolor, NULL, NULL);
          irr_scene_drawAll(smgr);
          irr_video_endScene(driver);

          int fps = irr_video_getFPS(driver);

          if (lastFPS != fps)
            {
              const char *driverName = irr_video_getName(driver);
              size_t capsize =
                snprintf(NULL, 0, "Irrlicht Engine - Quake 3 Map example [%s] FPS:%d",
                         driverName, fps);
              char *caption = (char*)malloc(capsize + 1);
              snprintf(caption, capsize + 1,
                       "Irrlicht Engine - Quake 3 Map example [%s] FPS:%d",
                       driverName, fps);

              irr_setWindowCaption(device, caption);
              lastFPS = fps;
            }
        }
      else
        irr_yield(device);
    }

  irr_drop(device);
  return 0;
}
