/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <cirrlicht/cirrlicht.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
  // ask user for driver
  irr_video_E_DRIVER_TYPE driverType;

  printf("Please select the driver you want for this example:\n"        \
         " (a) OpenGL 1.5\n (b) Direct3D 9.0c\n (c) Direct3D 8.1\n"     \
         " (d) Burning's Software Renderer\n (e) Software Renderer\n"   \
         " (f) NullDevice\n (otherKey) exit\n\n");

  char i = getchar();

  switch(i)
    {
    case 'a': driverType = irr_video_EDT_OPENGL; break;
    case 'b': driverType = irr_video_EDT_DIRECT3D9; break;
    case 'c': driverType = irr_video_EDT_DIRECT3D8; break;
    case 'd': driverType = irr_video_EDT_BURNINGSVIDEO; break;
    case 'e': driverType = irr_video_EDT_SOFTWARE; break;
    case 'f': driverType = irr_video_EDT_NULL; break;
    default: return 1;
    }

  // create device and exit if creation failed
  irr_core_dimension2d_u32 windowSize = {640, 480};
  irr_IrrlichtDevice *device =
    irr_createDevice(driverType, &windowSize, 16, false, false, false);

  if (!device)
    {
      return 1;  // could not create selected driver
    }

  // create engine and camera
  irr_setWindowCaption(device, "Custom Scene Node - Irrlicht Engine Demo");

  irr_video_IVideoDriver* driver = irr_getVideoDriver(device);
  irr_scene_ISceneManager* smgr = irr_getSceneManager(device);

  irr_core_vector3df position = {0, -40, 0};
  irr_core_vector3df lookat = {0, 0, 0};
  irr_scene_addCameraSceneNode(smgr, NULL, &position, &lookat, -1, true);

  // create our custom scene node
  irr_scene_ISceneNode* myNode;
  irr_core_aabbox3d_f32 box;
  irr_video_S3DVertex vertices[] =
    {
     // pos, normal, color, tcoords
     { {0, 0, 10}, {1, 1, 0}, MAKE_COLOR(255, 0, 255, 255), {0, 1} },
     { {10, 0, -10}, {1, 0, 0}, MAKE_COLOR(255, 255, 0, 255), {1, 1} },
     { {0, 20, 0}, {0, 1, 1}, MAKE_COLOR(255, 255, 255, 0), {1, 0} },
     { {-10, 0, -10}, {0, 0, 1}, MAKE_COLOR(255, 0, 255, 0), {0, 0} }
    };
  irr_video_SMaterial material = S_MATERIAL_DEFAULT;
  material.wireframe = false;
  material.lighting = false;

  irr_core_aabbox3d_reset(&box, &vertices[0].pos);
  for (int i=1; i<4; i++)
    {
      irr_core_aabbox3d_addInternalPoint(&box, &vertices[i].pos);
    }

  void customRender()
  {
    uint16_t indices[] = {0, 2, 3,    2, 1, 3,    1, 0, 3,    2, 0, 1};
    irr_video_setMaterial(driver, &material);
    irr_video_setTransform(driver,
                           irr_video_ETS_WORLD,
                           irr_scene_getAbsoluteTransformation(myNode));
    irr_video_drawVertexPrimitiveList(driver,
                                      &vertices[0], 4,
                                      &indices[0], 4,
                                      irr_video_EVT_STANDARD,
                                      irr_scene_EPT_TRIANGLES,
                                      irr_video_EIT_16BIT);
  }

  irr_core_aabbox3d_f32* customGetBoundingBox()
  {
    return &box;
  }

  uint32_t customGetMaterialCount()
  {
    return 1;
  }

  irr_video_SMaterial* customGetMaterial(unsigned int i)
  {
    return &material;
  }

  myNode = irr_scene_addCustomSceneNode(smgr, NULL, 666, NULL, NULL, NULL,
                                        customRender, customGetBoundingBox,
                                        customGetMaterialCount, customGetMaterial);

  // add rotation
  irr_core_vector3df rotation = {0.8, 0, 0.8};
  irr_scene_ISceneNodeAnimator* anim =
    irr_scene_createRotationAnimator(smgr, &rotation);

  if (anim)
    {
      irr_scene_addAnimator(myNode, anim);
      irr_drop(anim);
      anim = NULL;
    }

  // loop
  int frames = 0;
  irr_video_SColor bgcolor = MAKE_COLOR(0, 100, 100, 100);
  while (irr_run(device))
    {
      irr_video_beginScene(driver, true, true, &bgcolor, NULL, NULL);
      irr_scene_drawAll(smgr);
      irr_video_endScene(driver);

      frames++;
      if (frames == 100)
        {
          int fps = irr_video_getFPS(driver);
          const char *driverName = irr_video_getName(driver);
          size_t capsize =
            snprintf(NULL, 0, "Irrlicht Engine [%s] FPS:%d",
                     driverName, fps);
          char *caption = (char*)malloc(capsize + 1);
          snprintf(caption, capsize + 1,
                   "Irrlicht Engine [%s] FPS:%d",
                   driverName, fps);

          irr_setWindowCaption(device, caption);
          frames = 0;
        }
    }

  irr_drop(device);
  return 0;
}
