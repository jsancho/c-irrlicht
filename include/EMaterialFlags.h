/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_E_MATERIAL_FLAGS_H_INCLUDED__
#define __C_E_MATERIAL_FLAGS_H_INCLUDED__

typedef enum
  {
   irr_video_EMF_WIREFRAME = 0x1,
   irr_video_EMF_POINTCLOUD = 0x2,
   irr_video_EMF_GOURAUD_SHADING = 0x4,
   irr_video_EMF_LIGHTING = 0x8,
   irr_video_EMF_ZBUFFER = 0x10,
   irr_video_EMF_ZWRITE_ENABLE = 0x20,
   irr_video_EMF_BACK_FACE_CULLING = 0x40,
   irr_video_EMF_FRONT_FACE_CULLING = 0x80,
   irr_video_EMF_BILINEAR_FILTER = 0x100,
   irr_video_EMF_TRILINEAR_FILTER = 0x200,
   irr_video_EMF_ANISOTROPIC_FILTER = 0x400,
   irr_video_EMF_FOG_ENABLE = 0x800,
   irr_video_EMF_NORMALIZE_NORMALS = 0x1000,
   irr_video_EMF_TEXTURE_WRAP = 0x2000,
   irr_video_EMF_ANTI_ALIASING = 0x4000,
   irr_video_EMF_COLOR_MASK = 0x8000,
   irr_video_EMF_COLOR_MATERIAL = 0x10000,
   irr_video_EMF_USE_MIP_MAPS = 0x20000,
   irr_video_EMF_BLEND_OPERATION = 0x40000,
   irr_video_EMF_POLYGON_OFFSET = 0x80000
  } irr_video_E_MATERIAL_FLAG;

#endif
