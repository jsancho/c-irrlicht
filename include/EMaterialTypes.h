/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_E_MATERIAL_TYPES_H_INCLUDED__
#define __C_E_MATERIAL_TYPES_H_INCLUDED__

typedef enum
  {
   irr_video_EMT_SOLID = 0,
   irr_video_EMT_SOLID_2_LAYER,
   irr_video_EMT_LIGHTMAP,
   irr_video_EMT_LIGHTMAP_ADD,
   irr_video_EMT_LIGHTMAP_M2,
   irr_video_EMT_LIGHTMAP_M4,
   irr_video_EMT_LIGHTMAP_LIGHTING,
   irr_video_EMT_LIGHTMAP_LIGHTING_M2,
   irr_video_EMT_LIGHTMAP_LIGHTING_M4,
   irr_video_EMT_DETAIL_MAP,
   irr_video_EMT_SPHERE_MAP,
   irr_video_EMT_REFLECTION_2_LAYER,
   irr_video_EMT_TRANSPARENT_ADD_COLOR,
   irr_video_EMT_TRANSPARENT_ALPHA_CHANNEL,
   irr_video_EMT_TRANSPARENT_ALPHA_CHANNEL_REF,
   irr_video_EMT_TRANSPARENT_VERTEX_ALPHA,
   irr_video_EMT_TRANSPARENT_REFLECTION_2_LAYER,
   irr_video_EMT_NORMAL_MAP_SOLID,
   irr_video_EMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR,
   irr_video_EMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA,
   irr_video_EMT_PARALLAX_MAP_SOLID,
   irr_video_EMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR,
   irr_video_EMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA,
   irr_video_EMT_ONETEXTURE_BLEND,
   irr_video_EMT_FORCE_32BIT = 0x7fffffff
  } irr_video_E_MATERIAL_TYPE;

#endif
