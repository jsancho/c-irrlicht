/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_IRR_E_PRIMITIVE_TYPES_H_INCLUDED__
#define __C_IRR_E_PRIMITIVE_TYPES_H_INCLUDED__

//! Enumeration for all primitive types there are.
typedef enum
  {
   //! All vertices are non-connected points.
   irr_scene_EPT_POINTS=0,

   //! All vertices form a single connected line.
   irr_scene_EPT_LINE_STRIP,

   //! Just as LINE_STRIP, but the last and the first vertex is also connected.
   irr_scene_EPT_LINE_LOOP,

   //! Every two vertices are connected creating n/2 lines.
   irr_scene_EPT_LINES,

   //! After the first two vertices each vertex defines a new triangle.
   //! Always the two last and the new one form a new triangle.
   irr_scene_EPT_TRIANGLE_STRIP,

   //! After the first two vertices each vertex defines a new triangle.
   //! All around the common first vertex.
   irr_scene_EPT_TRIANGLE_FAN,

   //! Explicitly set all vertices for each triangle.
   irr_scene_EPT_TRIANGLES,

   //! After the first two vertices each further tw vetices create a quad with the preceding two.
   irr_scene_EPT_QUAD_STRIP,

   //! Every four vertices create a quad.
   irr_scene_EPT_QUADS,

   //! Just as LINE_LOOP, but filled.
   irr_scene_EPT_POLYGON,

   //! The single vertices are expanded to quad billboards on the GPU.
   irr_scene_EPT_POINT_SPRITES
  } irr_scene_E_PRIMITIVE_TYPE;

#endif
