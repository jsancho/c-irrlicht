/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_I_ANIMATED_MESH_MD2_H_INCLUDED__
#define __C_I_ANIMATED_MESH_MD2_H_INCLUDED__

typedef enum
  {
   irr_scene_EMAT_STAND = 0,
   irr_scene_EMAT_RUN,
   irr_scene_EMAT_ATTACK,
   irr_scene_EMAT_PAIN_A,
   irr_scene_EMAT_PAIN_B,
   irr_scene_EMAT_PAIN_C,
   irr_scene_EMAT_JUMP,
   irr_scene_EMAT_FLIP,
   irr_scene_EMAT_SALUTE,
   irr_scene_EMAT_FALLBACK,
   irr_scene_EMAT_WAVE,
   irr_scene_EMAT_POINT,
   irr_scene_EMAT_CROUCH_STAND,
   irr_scene_EMAT_CROUCH_WALK,
   irr_scene_EMAT_CROUCH_ATTACK,
   irr_scene_EMAT_CROUCH_PAIN,
   irr_scene_EMAT_CROUCH_DEATH,
   irr_scene_EMAT_DEATH_FALLBACK,
   irr_scene_EMAT_DEATH_FALLFORWARD,
   irr_scene_EMAT_DEATH_FALLBACKSLOW,
   irr_scene_EMAT_BOOM,
   irr_scene_EMAT_COUNT
  } irr_scene_EMD2_ANIMATION_TYPE;

#endif
