/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_I_FILE_ARCHIVE_H_INCLUDED__
#define __C_I_FILE_ARCHIVE_H_INCLUDED__

#include "irrTypes.h"

typedef enum
  {
   //! A PKZIP archive
   irr_io_EFAT_ZIP     = MAKE_CIRR_ID('Z','I','P', 0),

   //! A gzip archive
   irr_io_EFAT_GZIP    = MAKE_CIRR_ID('g','z','i','p'),

   //! A virtual directory
   irr_io_EFAT_FOLDER  = MAKE_CIRR_ID('f','l','d','r'),

   //! An ID Software PAK archive
   irr_io_EFAT_PAK     = MAKE_CIRR_ID('P','A','K', 0),

   //! A Nebula Device archive
   irr_io_EFAT_NPK     = MAKE_CIRR_ID('N','P','K', 0),

   //! A Tape ARchive
   irr_io_EFAT_TAR     = MAKE_CIRR_ID('T','A','R', 0),

   //! A wad Archive, Quake2, Halflife
   irr_io_EFAT_WAD     = MAKE_CIRR_ID('W','A','D', 0),

   //! The type of this archive is unknown
   irr_io_EFAT_UNKNOWN = MAKE_CIRR_ID('u','n','k','n')
  } irr_io_E_FILE_ARCHIVE_TYPE;

typedef void irr_io_IFileArchive;

#endif
