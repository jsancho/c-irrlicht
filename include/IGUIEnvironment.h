/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_I_GUI_ENVIRONMENT_H_INCLUDED__
#define __C_I_GUI_ENVIRONMENT_H_INCLUDED__

#include <inttypes.h>
#include <stdbool.h>

#include "IGUIElement.h"
#include "IGUIStaticText.h"
#include "rect.h"

typedef void irr_gui_IGUIEnvironment;

#ifdef __cplusplus
extern "C" {
#endif

  irr_gui_IGUIStaticText*
  irr_gui_addStaticText(irr_gui_IGUIEnvironment* guienv,
                        const char* text,
                        const irr_core_rect_s32* rectangle,
                        bool border,
                        bool wordWrap,
                        irr_gui_IGUIElement* parent,
                        int32_t id,
                        bool fillBackground);

  void
  irr_gui_drawAll(irr_gui_IGUIEnvironment* guienv);

#ifdef __cplusplus
}
#endif

#endif
