/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_I_SCENE_MANAGER_H_INCLUDED__
#define __C_I_SCENE_MANAGER_H_INCLUDED__

#include <inttypes.h>
#include <math.h>
#include <stdbool.h>

#include "aabbox3d.h"
#include "IAnimatedMesh.h"
#include "IAnimatedMeshSceneNode.h"
#include "ICameraSceneNode.h"
#include "IMeshSceneNode.h"
#include "ISceneNode.h"
#include "ISceneNodeAnimator.h"
#include "SKeyMap.h"
#include "SMaterial.h"
#include "vector3d.h"

typedef void irr_scene_ISceneManager;

#ifdef __cplusplus
extern "C" {
#endif

  irr_scene_IAnimatedMeshSceneNode*
  irr_scene_addAnimatedMeshSceneNode(irr_scene_ISceneManager* smgr,
                                     irr_scene_IAnimatedMesh* mesh,
                                     irr_scene_ISceneNode* parent,
                                     int32_t id,
                                     irr_core_vector3df* position,
                                     irr_core_vector3df* rotation,
                                     irr_core_vector3df* scale,
                                     bool alsoAddIfMeshPointerZero);

  irr_scene_ICameraSceneNode*
  irr_scene_addCameraSceneNode(irr_scene_ISceneManager* smgr,
                               irr_scene_ISceneNode* parent,
                               irr_core_vector3df* position,
                               irr_core_vector3df* lookat,
                               int32_t id,
                               bool makeActive);

  irr_scene_ICameraSceneNode*
  irr_scene_addCameraSceneNodeFPS(irr_scene_ISceneManager* smgr,
                                  irr_scene_ISceneNode* parent,
                                  float_t rotateSpeed,
                                  float_t moveSpeed,
                                  int32_t id,
                                  irr_SkeyMap* keyMapArray,
                                  int32_t keyMapSize,
                                  bool noVerticalMovement,
                                  float_t jumpSpeed,
                                  bool invertMouse,
                                  bool makeActive);

  irr_scene_ISceneNode*
  irr_scene_addCustomSceneNode(irr_scene_ISceneManager* smgr,
                               irr_scene_ISceneNode* parent,
                               int32_t id,
                               irr_core_vector3df* position,
                               irr_core_vector3df* rotation,
                               irr_core_vector3df* scale,
                               void (*render)(),
                               irr_core_aabbox3d_f32* (*getBoundingBox)(),
                               uint32_t (*getMaterialCount)(),
                               irr_video_SMaterial* (*getMaterial)(unsigned int i));

  irr_scene_IMeshSceneNode*
  irr_scene_addOctreeSceneNode(irr_scene_ISceneManager* smgr,
                               irr_scene_IAnimatedMesh* mesh,
                               irr_scene_ISceneNode* parent,
                               int32_t id,
                               int32_t minimalPolysPerNode,
                               bool alsoAddIfMeshPointerZero);

  irr_scene_ISceneNodeAnimator*
  irr_scene_createRotationAnimator(irr_scene_ISceneManager* smgr,
                                   irr_core_vector3df* rotationSpeed);

  void
  irr_scene_drawAll(irr_scene_ISceneManager* smgr);

  irr_scene_IAnimatedMesh*
  irr_scene_getMesh(irr_scene_ISceneManager* smgr,
                    const char* filename);

  irr_scene_ISceneNode*
  irr_scene_getRootSceneNode(irr_scene_ISceneManager* smgr);

#ifdef __cplusplus
}
#endif

#endif
