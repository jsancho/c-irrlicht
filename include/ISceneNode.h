/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_I_SCENE_NODE_H_INCLUDED__
#define __C_I_SCENE_NODE_H_INCLUDED__

#include <inttypes.h>
#include <stdbool.h>

#include "EMaterialFlags.h"
#include "ISceneNodeAnimator.h"
#include "ITexture.h"
#include "matrix4.h"
#include "vector3d.h"

typedef void irr_scene_ISceneNode;

#ifdef __cplusplus
extern "C" {
#endif

  void
  irr_scene_addAnimator(irr_scene_ISceneNode* node,
                        irr_scene_ISceneNodeAnimator* animator);

  irr_core_matrix4*
  irr_scene_getAbsoluteTransformation(irr_scene_ISceneNode* node);

  void
  irr_scene_setMaterialFlag(irr_scene_ISceneNode* node,
                            irr_video_E_MATERIAL_FLAG flag,
                            bool newvalue);

  void
  irr_scene_setMaterialTexture(irr_scene_ISceneNode* node,
                               uint32_t textureLayer,
                               irr_video_ITexture* texture);

  void
  irr_scene_setPosition(irr_scene_ISceneNode* node,
                        irr_core_vector3df* newpos);

#ifdef __cplusplus
}
#endif

#endif
