/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_I_VIDEO_DRIVER_H_INCLUDED__
#define __C_I_VIDEO_DRIVER_H_INCLUDED__

#include <inttypes.h>
#include <stdbool.h>

#include "EPrimitiveTypes.h"
#include "ITexture.h"
#include "matrix4.h"
#include "rect.h"
#include "S3DVertex.h"
#include "SColor.h"
#include "SExposedVideoData.h"
#include "SMaterial.h"
#include "SVertexIndex.h"

//! enumeration for geometry transformation states
typedef enum
  {
   //! View transformation
   irr_video_ETS_VIEW = 0,
   //! World transformation
   irr_video_ETS_WORLD,
   //! Projection transformation
   irr_video_ETS_PROJECTION,
   //! Texture transformation
   irr_video_ETS_TEXTURE_0,
   //! Texture transformation
   irr_video_ETS_TEXTURE_1,
   //! Texture transformation
   irr_video_ETS_TEXTURE_2,
   //! Texture transformation
   irr_video_ETS_TEXTURE_3,
#if _C_IRR_MATERIAL_MAX_TEXTURES_>4
   //! Texture transformation
   irr_video_ETS_TEXTURE_4,
#if _C_IRR_MATERIAL_MAX_TEXTURES_>5
   //! Texture transformation
   irr_video_ETS_TEXTURE_5,
#if _C_IRR_MATERIAL_MAX_TEXTURES_>6
   //! Texture transformation
   irr_video_ETS_TEXTURE_6,
#if _C_IRR_MATERIAL_MAX_TEXTURES_>7
   //! Texture transformation
   irr_video_ETS_TEXTURE_7,
#endif
#endif
#endif
#endif
   //! Not used
   irr_video_ETS_COUNT
  } irr_video_E_TRANSFORMATION_STATE;

// types
typedef void irr_video_IVideoDriver;

#ifdef __cplusplus
extern "C" {
#endif

  int
  irr_video_beginScene(irr_video_IVideoDriver* driver,
                       bool backBuffer,
                       bool zBuffer,
                       const irr_video_SColor* color,
                       irr_video_SExposedVideoData* videoData,
                       const irr_core_rect_s32* sourceRect);

  void
  irr_video_drawVertexPrimitiveList(irr_video_IVideoDriver* driver,
                                    const void* vertices,
                                    uint32_t vertexCount,
                                    const void* indexList,
                                    uint32_t primCount,
                                    irr_video_E_VERTEX_TYPE vType,
                                    irr_scene_E_PRIMITIVE_TYPE pType,
                                    irr_video_E_INDEX_TYPE iType);

  int
  irr_video_endScene(irr_video_IVideoDriver* driver);

  int
  irr_video_getFPS(irr_video_IVideoDriver* driver);

  const char*
  irr_video_getName(irr_video_IVideoDriver* driver);

  irr_video_ITexture*
  irr_video_getTexture(irr_video_IVideoDriver* driver,
                       const char* filename);

  void
  irr_video_setMaterial(irr_video_IVideoDriver* driver,
                        irr_video_SMaterial* material);

  void
  irr_video_setTransform(irr_video_IVideoDriver* driver,
                         irr_video_E_TRANSFORMATION_STATE state,
                         irr_core_matrix4* mat);

#ifdef __cplusplus
}
#endif

#endif
