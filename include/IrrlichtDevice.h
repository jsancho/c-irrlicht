/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_IRRLICHT_DEVICE_H_INCLUDED__
#define __C_IRRLICHT_DEVICE_H_INCLUDED__

#include <stdbool.h>

#include "ICursorControl.h"
#include "IFileSystem.h"
#include "IGUIEnvironment.h"
#include "ISceneManager.h"
#include "IVideoDriver.h"

typedef void irr_IrrlichtDevice;

#ifdef __cplusplus
extern "C" {
#endif

  irr_gui_ICursorControl*
  irr_getCursorControl(irr_IrrlichtDevice* device);

  irr_io_IFileSystem*
  irr_getFileSystem(irr_IrrlichtDevice* device);

  irr_gui_IGUIEnvironment*
  irr_getGUIEnvironment(irr_IrrlichtDevice* device);

  irr_scene_ISceneManager*
  irr_getSceneManager(irr_IrrlichtDevice* device);

  irr_video_IVideoDriver*
  irr_getVideoDriver(irr_IrrlichtDevice* device);

  bool
  irr_isWindowActive(irr_IrrlichtDevice* device);

  void
  irr_setWindowCaption(irr_IrrlichtDevice* device,
                       const char* text);

  bool
  irr_run(irr_IrrlichtDevice* device);

  void
  irr_yield(irr_IrrlichtDevice* device);

#ifdef __cplusplus
}
#endif

#endif
