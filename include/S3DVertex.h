/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_IRR_S_3D_VERTEX_H_INCLUDED__
#define __C_IRR_S_3D_VERTEX_H_INCLUDED__

#include "SColor.h"
#include "vector2d.h"
#include "vector3d.h"

//! Enumeration for all vertex types there are.
typedef enum
  {
   //! Standard vertex type used by the Irrlicht engine, video::S3DVertex.
   irr_video_EVT_STANDARD = 0,

   //! Vertex with two texture coordinates, video::S3DVertex2TCoords.
   /** Usually used for geometry with lightmaps or other special materials. */
   irr_video_EVT_2TCOORDS,

   //! Vertex with a tangent and binormal vector, video::S3DVertexTangents.
   /** Usually used for tangent space normal mapping. */
   irr_video_EVT_TANGENTS
  } irr_video_E_VERTEX_TYPE;

typedef struct
{
  irr_core_vector3df pos;
  irr_core_vector3df normal;
  irr_video_SColor color;
  irr_core_vector2df tCoords;
} irr_video_S3DVertex;

#endif
