/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_IRR_S_MATERIAL_H_INCLUDED__
#define __C_IRR_S_MATERIAL_H_INCLUDED__

#include <inttypes.h>
#include <math.h>
#include <stdbool.h>

#include "EMaterialTypes.h"
#include "irrMath.h"
#include "SColor.h"
#include "SMaterialLayer.h"

#define MATERIAL_MAX_TEXTURES _IRR_MATERIAL_MAX_TEXTURES_

typedef enum
  {
   irr_video_EBO_NONE = 0,
   irr_video_EBO_ADD,
   irr_video_EBO_SUBTRACT,
   irr_video_EBO_REVSUBTRACT,
   irr_video_EBO_MIN,
   irr_video_EBO_MAX,
   irr_video_EBO_MIN_FACTOR,
   irr_video_EBO_MAX_FACTOR,
   irr_video_EBO_MIN_ALPHA,
   irr_video_EBO_MAX_ALPHA
  } irr_video_E_BLEND_OPERATION;

typedef enum
  {
   irr_video_ECFN_NEVER = 0,
   irr_video_ECFN_LESSEQUAL = 1,
   irr_video_ECFN_EQUAL = 2,
   irr_video_ECFN_LESS,
   irr_video_ECFN_NOTEQUAL,
   irr_video_ECFN_GREATEREQUAL,
   irr_video_ECFN_GREATER,
   irr_video_ECFN_ALWAYS
  } irr_video_E_COMPARISON_FUNC;

typedef enum
  {
   irr_video_ECP_NONE = 0,
   irr_video_ECP_ALPHA = 1,
   irr_video_ECP_RED = 2,
   irr_video_ECP_GREEN = 4,
   irr_video_ECP_BLUE = 8,
   irr_video_ECP_RGB = 14,
   irr_video_ECP_ALL = 15
  } irr_video_E_COLOR_PLANE;

typedef enum
  {
   irr_video_EAAM_OFF = 0,
   irr_video_EAAM_SIMPLE = 1,
   irr_video_EAAM_QUALITY = 3,
   irr_video_EAAM_LINE_SMOOTH = 4,
   irr_video_EAAM_POINT_SMOOTH = 8,
   irr_video_EAAM_FULL_BASIC = 15,
   irr_video_EAAM_ALPHA_TO_COVERAGE = 16
  } irr_video_E_ANTI_ALIASING_MODE;

typedef enum
  {
   irr_video_ECM_NONE = 0,
   irr_video_ECM_DIFFUSE,
   irr_video_ECM_AMBIENT,
   irr_video_ECM_EMISSIVE,
   irr_video_ECM_SPECULAR,
   irr_video_ECM_DIFFUSE_AND_AMBIENT
  } irr_video_E_COLOR_MATERIAL;

typedef enum
  {
   irr_video_EPO_BACK = 0,
   irr_video_EPO_FRONT = 1
  } irr_video_E_POLYGON_OFFSET;

typedef struct
{
  irr_video_SMaterialLayer textureLayer[MATERIAL_MAX_TEXTURES];
  irr_video_E_MATERIAL_TYPE materialType;
  irr_video_SColor ambientColor;
  irr_video_SColor diffuseColor;
  irr_video_SColor emissiveColor;
  irr_video_SColor specularColor;
  float_t shininess;
  float_t materialTypeParam;
  float_t materialTypeParam2;
  float_t thickness;
  uint8_t zBuffer;
  uint8_t antiAliasing;
  uint8_t colorMask:4;
  uint8_t colorMaterial:3;
  irr_video_E_BLEND_OPERATION blendOperation:4;
  uint8_t polygonOffsetFactor:3;
  irr_video_E_POLYGON_OFFSET polygonOffsetDirection:1;
  bool wireframe:1;
  bool pointCloud:1;
  bool gouraudShading:1;
  bool lighting:1;
  bool zWriteEnable:1;
  bool backfaceCulling:1;
  bool frontfaceCulling:1;
  bool fogEnable:1;
  bool normalizeNormals:1;
  bool useMipMaps:1;
} irr_video_SMaterial;

#define S_MATERIAL_DEFAULT                            \
{                                                     \
  .materialType = irr_video_EMT_SOLID,                \
  .ambientColor = {255, 255, 255, 255},               \
  .diffuseColor = {255, 255, 255, 255},               \
  .emissiveColor = {0, 0, 0, 0},                      \
  .specularColor = {255, 255, 255, 255},              \
  .shininess = 0,                                     \
  .materialTypeParam = 0,                             \
  .materialTypeParam2 = 0,                            \
  .thickness = 1,                                     \
  .zBuffer = irr_video_ECFN_LESSEQUAL,                \
  .antiAliasing = irr_video_EAAM_SIMPLE,              \
  .colorMask = irr_video_ECP_ALL,                     \
  .colorMaterial = irr_video_ECM_DIFFUSE,             \
  .blendOperation = irr_video_EBO_NONE,               \
  .polygonOffsetFactor = 0,                           \
  .polygonOffsetDirection = irr_video_EPO_FRONT,      \
  .wireframe = false,                                 \
  .pointCloud = false,                                \
  .gouraudShading = true,                             \
  .lighting = true,                                   \
  .zWriteEnable = true,                               \
  .backfaceCulling = true,                            \
  .frontfaceCulling = false,                          \
  .fogEnable = false,                                 \
  .normalizeNormals = false,                          \
  .useMipMaps = true                                  \
}

#endif
