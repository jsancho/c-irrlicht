/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#ifndef __C_IRRLICHT_H_INCLUDED__
#define __C_IRRLICHT_H_INCLUDED__

#include <inttypes.h>
#include <stdbool.h>

#include "aabbox3d.h"
#include "CIrrCompileConfig.h"
#include "dimension2d.h"
#include "EDriverTypes.h"
#include "EMaterialFlags.h"
#include "EMaterialTypes.h"
#include "EPrimitiveTypes.h"
#include "IAnimatedMeshMD2.h"
#include "IAnimatedMeshSceneNode.h"
#include "ICameraSceneNode.h"
#include "ICursorControl.h"
#include "IFileArchive.h"
#include "IFileSystem.h"
#include "IGUIEnvironment.h"
#include "IGUIElement.h"
#include "IGUIStaticText.h"
#include "IMeshSceneNode.h"
#include "IReferenceCounted.h"
#include "IrrlichtDevice.h"
#include "irrMath.h"
#include "irrTypes.h"
#include "ISceneManager.h"
#include "ISceneNode.h"
#include "ISceneNodeAnimator.h"
#include "ITexture.h"
#include "IVideoDriver.h"
#include "matrix4.h"
#include "rect.h"
#include "S3DVertex.h"
#include "SColor.h"
#include "SExposedVideoData.h"
#include "SKeyMap.h"
#include "SMaterial.h"
#include "SMaterialLayer.h"
#include "SVertexIndex.h"
#include "vector2d.h"
#include "vector3d.h"

#ifdef __cplusplus
extern "C" {
#endif

  irr_IrrlichtDevice*
  irr_createDevice(irr_video_E_DRIVER_TYPE deviceType,
                   const irr_core_dimension2d_u32* windowSize,
                   uint32_t bits,
                   bool fullscreen,
                   bool stencilbuffer,
                   bool vsync);

#ifdef __cplusplus
}
#endif

#endif
