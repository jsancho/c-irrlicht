/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include "cirrlicht.h"

extern "C" {
  irr_IrrlichtDevice*
  irr_createDevice(irr_video_E_DRIVER_TYPE deviceType,
                   const irr_core_dimension2d_u32* windowSize,
                   uint32_t bits,
                   bool fullscreen,
                   bool stencilbuffer,
                   bool vsync)
  {
    irr::IrrlichtDevice *device =
      irr::createDevice((irr::video::E_DRIVER_TYPE)deviceType,
                        *(irr::core::dimension2d<irr::u32>*)windowSize,
                        bits, fullscreen, stencilbuffer, vsync);
    return (irr_IrrlichtDevice*)device;
  }
}
