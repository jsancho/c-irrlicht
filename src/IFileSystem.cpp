/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include "IFileSystem.h"

extern "C" {
  int
  irr_io_addFileArchive(irr_io_IFileSystem* filesystem,
                        const char* filename,
                        bool ignoreCase,
                        bool ignorePaths,
                        irr_io_E_FILE_ARCHIVE_TYPE archiveType,
                        const char* password,
                        irr_io_IFileArchive** retArchive)
  {
    return ((irr::io::IFileSystem*)filesystem)
      ->addFileArchive(filename,
                       ignoreCase,
                       ignorePaths,
                       (irr::io::E_FILE_ARCHIVE_TYPE)archiveType,
                       password,
                       (irr::io::IFileArchive**)retArchive);
  }

}
