/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include <wchar.h>
#include "IGUIEnvironment.h"

extern "C" {
  irr_gui_IGUIStaticText*
  irr_gui_addStaticText(irr_gui_IGUIEnvironment* guienv,
                        const char* text,
                        const irr_core_rect_s32* rectangle,
                        bool border,
                        bool wordWrap,
                        irr_gui_IGUIElement* parent,
                        int32_t id,
                        bool fillBackground)
  {
    // Convert to wide char text
    wchar_t *wtext = (wchar_t*)malloc((strlen(text) + 1) * sizeof(wchar_t));
    mbstowcs(wtext, text, strlen(text) + 1);

    // Add static text
    irr::gui::IGUIStaticText *staticText =
      ((irr::gui::IGUIEnvironment*)guienv)
      ->addStaticText(wtext,
                      *(irr::core::rect<irr::s32>*)rectangle,
                      border,
                      wordWrap,
                      (irr::gui::IGUIElement*)parent,
                      id,
                      fillBackground);
    return staticText;
  }

  void
  irr_gui_drawAll(irr_gui_IGUIEnvironment* guienv)
  {
    ((irr::gui::IGUIEnvironment*)guienv)->drawAll();
  }
}
