/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include "ISceneManager.h"

class CustomSceneNode : public irr::scene::ISceneNode
{
  void (*Render)();
  irr_core_aabbox3d_f32* (*GetBoundingBox)();
  uint32_t (*GetMaterialCount)();
  irr_video_SMaterial* (*GetMaterial)(unsigned int i);

public:
  CustomSceneNode(irr::scene::ISceneNode* parent,
                  irr::scene::ISceneManager* smgr,
                  irr::s32 id,
                  const irr::core::vector3df& position,
                  const irr::core::vector3df& rotation,
                  const irr::core::vector3df& scale,
                  void (*render)(),
                  irr_core_aabbox3d_f32* (*getBoundingBox)(),
                  uint32_t (*getMaterialCount)(),
                  irr_video_SMaterial* (*getMaterial)(unsigned int i))
  : irr::scene::ISceneNode(parent, smgr, id, position, rotation, scale)
  {
    Render = render;
    GetBoundingBox = getBoundingBox;
    GetMaterialCount = getMaterialCount;
    GetMaterial = getMaterial;
  }

  virtual void OnRegisterSceneNode()
  {
    if (IsVisible) {
      SceneManager->registerNodeForRendering(this);
    }

    ISceneNode::OnRegisterSceneNode();
  }

  virtual void render()
  {
    Render();
  }

  virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const
  {
    return *(irr::core::aabbox3d<irr::f32>*)GetBoundingBox();
  }

  virtual irr::u32 getMaterialCount() const
  {
    return GetMaterialCount();
  }

  virtual irr::video::SMaterial& getMaterial(irr::u32 i)
  {
    return *(irr::video::SMaterial*)GetMaterial(i);
  }
};

extern "C" {
  irr_scene_IAnimatedMeshSceneNode*
  irr_scene_addAnimatedMeshSceneNode(irr_scene_ISceneManager* smgr,
                                     irr_scene_IAnimatedMesh* mesh,
                                     irr_scene_ISceneNode* parent,
                                     int32_t id,
                                     irr_core_vector3df* position,
                                     irr_core_vector3df* rotation,
                                     irr_core_vector3df* scale,
                                     bool alsoAddIfMeshPointerZero)
  {
    return (irr_scene_IAnimatedMeshSceneNode*)
      ((irr::scene::ISceneManager*)smgr)
      ->addAnimatedMeshSceneNode((irr::scene::IAnimatedMesh*)mesh,
                                 (irr::scene::ISceneNode*)parent,
                                 id,
                                 position ? *(irr::core::vector3df*)position : irr::core::vector3df(0, 0, 0),
                                 rotation ? *(irr::core::vector3df*)rotation : irr::core::vector3df(0, 0, 0),
                                 scale ? *(irr::core::vector3df*)scale : irr::core::vector3df(1, 1, 1),
                                 alsoAddIfMeshPointerZero);
  }

  irr_scene_ICameraSceneNode*
  irr_scene_addCameraSceneNode(irr_scene_ISceneManager* smgr,
                               irr_scene_ISceneNode* parent,
                               irr_core_vector3df* position,
                               irr_core_vector3df* lookat,
                               int32_t id,
                               bool makeActive)
  {
    return (irr_scene_ICameraSceneNode*)
      ((irr::scene::ISceneManager*)smgr)
      ->addCameraSceneNode((irr::scene::ISceneNode*)parent,
                           position ? *(irr::core::vector3df*)position : irr::core::vector3df(0, 0, 0),
                           lookat ? *(irr::core::vector3df*)lookat : irr::core::vector3df(0, 0, 100),
                           id,
                           makeActive);
  }

  irr_scene_ICameraSceneNode*
  irr_scene_addCameraSceneNodeFPS(irr_scene_ISceneManager* smgr,
                                  irr_scene_ISceneNode* parent,
                                  float_t rotateSpeed,
                                  float_t moveSpeed,
                                  int32_t id,
                                  irr_SkeyMap* keyMapArray,
                                  int32_t keyMapSize,
                                  bool noVerticalMovement,
                                  float_t jumpSpeed,
                                  bool invertMouse,
                                  bool makeActive)
  {
    return ((irr::scene::ISceneManager*)smgr)
      ->addCameraSceneNodeFPS((irr::scene::ISceneNode*)parent,
                              rotateSpeed,
                              moveSpeed,
                              id,
                              (irr::SKeyMap*)keyMapArray,
                              keyMapSize,
                              noVerticalMovement,
                              jumpSpeed,
                              invertMouse,
                              makeActive);
  }

  irr_scene_ISceneNode*
  irr_scene_addCustomSceneNode(irr_scene_ISceneManager* smgr,
                               irr_scene_ISceneNode* parent,
                               int32_t id,
                               irr_core_vector3df* position,
                               irr_core_vector3df* rotation,
                               irr_core_vector3df* scale,
                               void (*render)(),
                               irr_core_aabbox3d_f32* (*getBoundingBox)(),
                               uint32_t (*getMaterialCount)(),
                               irr_video_SMaterial* (*getMaterial)(unsigned int i))
  {
    if (parent == NULL)
      {
        parent = irr_scene_getRootSceneNode(smgr);
      }

    CustomSceneNode *node =
      new CustomSceneNode((irr::scene::ISceneNode*)parent,
                          (irr::scene::ISceneManager*)smgr,
                          id,
                          position ? *(irr::core::vector3df*)position : irr::core::vector3df(0, 0, 0),
                          rotation ? *(irr::core::vector3df*)rotation : irr::core::vector3df(0, 0, 0),
                          scale ? *(irr::core::vector3df*)scale : irr::core::vector3df(1, 1, 1),
                          render,
                          getBoundingBox,
                          getMaterialCount,
                          getMaterial);
    return node;
  }

  irr_scene_IMeshSceneNode*
  irr_scene_addOctreeSceneNode(irr_scene_ISceneManager* smgr,
                               irr_scene_IAnimatedMesh* mesh,
                               irr_scene_ISceneNode* parent,
                               int32_t id,
                               int32_t minimalPolysPerNode,
                               bool alsoAddIfMeshPointerZero)
  {
    return ((irr::scene::ISceneManager*)smgr)
      ->addOctreeSceneNode((irr::scene::IAnimatedMesh*)mesh,
                           (irr::scene::ISceneNode*)parent,
                           id,
                           minimalPolysPerNode,
                           alsoAddIfMeshPointerZero);
  }

  irr_scene_ISceneNodeAnimator*
  irr_scene_createRotationAnimator(irr_scene_ISceneManager* smgr,
                                   irr_core_vector3df* rotationSpeed)
  {
    return ((irr::scene::ISceneManager*)smgr)
      ->createRotationAnimator(*(irr::core::vector3df*)rotationSpeed);
  }

  void
  irr_scene_drawAll(irr_scene_ISceneManager* smgr)
  {
    ((irr::scene::ISceneManager*)smgr)->drawAll();
  }


  irr_scene_IAnimatedMesh*
  irr_scene_getMesh(irr_scene_ISceneManager* smgr,
                    const char* filename)
  {
    return (irr_scene_IAnimatedMesh*)
      ((irr::scene::ISceneManager*)smgr)->getMesh(filename);
  }

  irr_scene_ISceneNode*
  irr_scene_getRootSceneNode(irr_scene_ISceneManager* smgr)
  {
    return (irr_scene_ISceneNode*)
      ((irr::scene::ISceneManager*)smgr)->getRootSceneNode();
  }
}
