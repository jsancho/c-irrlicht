/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include "ISceneNode.h"

extern "C" {
  void
  irr_scene_addAnimator(irr_scene_ISceneNode* node,
                        irr_scene_ISceneNodeAnimator* animator)
  {
    ((irr::scene::ISceneNode*)node)
      ->addAnimator((irr::scene::ISceneNodeAnimator*)animator);
  }

  irr_core_matrix4*
  irr_scene_getAbsoluteTransformation(irr_scene_ISceneNode* node)
  {
    return (irr_core_matrix4*)
      &((irr::scene::ISceneNode*)node)->getAbsoluteTransformation();
  }

  void
  irr_scene_setMaterialFlag(irr_scene_ISceneNode* node,
                            irr_video_E_MATERIAL_FLAG flag,
                            bool newvalue)
  {
    ((irr::scene::ISceneNode*)node)
      ->setMaterialFlag((irr::video::E_MATERIAL_FLAG)flag,
                        newvalue);
  }

  void
  irr_scene_setMaterialTexture(irr_scene_ISceneNode* node,
                               uint32_t textureLayer,
                               irr_video_ITexture* texture)
  {
    ((irr::scene::ISceneNode*)node)
      ->setMaterialTexture(textureLayer,
                           (irr::video::ITexture*)texture);
  }

  void
  irr_scene_setPosition(irr_scene_ISceneNode* node,
                        irr_core_vector3df* newpos)
  {
    ((irr::scene::ISceneNode*)node)
      ->setPosition(*(irr::core::vector3df*)newpos);
  }

}
