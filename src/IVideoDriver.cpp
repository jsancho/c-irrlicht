/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include <wchar.h>
#include "IVideoDriver.h"

extern "C" {
  int
  irr_video_beginScene(irr_video_IVideoDriver* driver,
                       bool backBuffer,
                       bool zBuffer,
                       const irr_video_SColor* color,
                       irr_video_SExposedVideoData* videoData,
                       const irr_core_rect_s32* sourceRect)
  {
    // Video data
    // TODO
    irr::video::SExposedVideoData vdata = irr::video::SExposedVideoData();

    // Begin scene
    return ((irr::video::IVideoDriver*)driver)
      ->beginScene(backBuffer,
                   zBuffer,
                   *(irr::video::SColor*)color,
                   vdata,
                   (irr::core::rect<irr::s32>*)sourceRect);
  }

  void
  irr_video_drawVertexPrimitiveList(irr_video_IVideoDriver* driver,
                                    const void* vertices,
                                    uint32_t vertexCount,
                                    const void* indexList,
                                    uint32_t primCount,
                                    irr_video_E_VERTEX_TYPE vType,
                                    irr_scene_E_PRIMITIVE_TYPE pType,
                                    irr_video_E_INDEX_TYPE iType)
  {
    ((irr::video::IVideoDriver*)driver)
      ->drawVertexPrimitiveList(vertices, vertexCount,
                                indexList, primCount,
                                (irr::video::E_VERTEX_TYPE)vType,
                                (irr::scene::E_PRIMITIVE_TYPE)pType,
                                (irr::video::E_INDEX_TYPE)iType);
  }

  int
  irr_video_endScene(irr_video_IVideoDriver* driver)
  {
    return ((irr::video::IVideoDriver*)driver)->endScene();
  }

  int
  irr_video_getFPS(irr_video_IVideoDriver* driver)
  {
    return ((irr::video::IVideoDriver*)driver)->getFPS();
  }

  const char*
  irr_video_getName(irr_video_IVideoDriver* driver)
  {
    const wchar_t *wname = ((irr::video::IVideoDriver*)driver)->getName();
    size_t nbytes = wcslen(wname) + 1;
    char *name = (char*)malloc(nbytes);
    wcstombs(name, wname, nbytes);
    return name;
  }

  irr_video_ITexture*
  irr_video_getTexture(irr_video_IVideoDriver* driver,
                       const char* filename)
  {
    return (irr_video_ITexture*)
      ((irr::video::IVideoDriver*)driver)->getTexture(filename);
  }

  void
  irr_video_setMaterial(irr_video_IVideoDriver* driver,
                        irr_video_SMaterial* material)
  {
    ((irr::video::IVideoDriver*)driver)->setMaterial(*(irr::video::SMaterial*)material);
  }

  void
  irr_video_setTransform(irr_video_IVideoDriver* driver,
                         irr_video_E_TRANSFORMATION_STATE state,
                         irr_core_matrix4* mat)
  {
    ((irr::video::IVideoDriver*)driver)
      ->setTransform((irr::video::E_TRANSFORMATION_STATE)state,
                     *(irr::core::matrix4*)mat);
  }
}
