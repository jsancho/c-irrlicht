/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include <wchar.h>
#include "IrrlichtDevice.h"

extern "C" {
  irr_gui_ICursorControl*
  irr_getCursorControl(irr_IrrlichtDevice* device)
  {
    return ((irr::IrrlichtDevice*)device)->getCursorControl();
  }

  irr_io_IFileSystem*
  irr_getFileSystem(irr_IrrlichtDevice* device)
  {
    return ((irr::IrrlichtDevice*)device)->getFileSystem();
  }

  irr_gui_IGUIEnvironment*
  irr_getGUIEnvironment(irr_IrrlichtDevice* device)
  {
    return ((irr::IrrlichtDevice*)device)->getGUIEnvironment();
  }

  irr_scene_ISceneManager*
  irr_getSceneManager(irr_IrrlichtDevice* device)
  {
    return ((irr::IrrlichtDevice*)device)->getSceneManager();
  }

  irr_video_IVideoDriver*
  irr_getVideoDriver(irr_IrrlichtDevice* device)
  {
    return ((irr::IrrlichtDevice*)device)->getVideoDriver();
  }

  bool
  irr_isWindowActive(irr_IrrlichtDevice* device)
  {
    return ((irr::IrrlichtDevice*)device)->isWindowActive();
  }

  void
  irr_setWindowCaption(irr_IrrlichtDevice* device,
                       const char* text)
  {
    wchar_t *wtext = (wchar_t*)malloc((strlen(text) + 1) * sizeof(wchar_t));
    mbstowcs(wtext, text, strlen(text) + 1);
    ((irr::IrrlichtDevice*)device)->setWindowCaption(wtext);
  }

  bool
  irr_run(irr_IrrlichtDevice* device)
  {
    return ((irr::IrrlichtDevice*)device)->run();
  }

  void
  irr_yield(irr_IrrlichtDevice* device)
  {
    ((irr::IrrlichtDevice*)device)->yield();
  }

}
