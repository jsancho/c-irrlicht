/* c-irrlicht --- C bindings for Irrlicht Engine

   Copyright (C) 2019 Javier Sancho <jsf@jsancho.org>

   This file is part of c-irrlicht.

   c-irrlicht is free software; you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as
   published by the Free Software Foundation; either version 3 of the
   License, or (at your option) any later version.

   c-irrlicht is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with guile-irrlicht.  If not, see
   <http://www.gnu.org/licenses/>.
*/

#include <irrlicht/irrlicht.h>
#include "aabbox3d.h"

extern "C" {
  void
  irr_core_aabbox3d_addInternalPoint(irr_core_aabbox3d_f32* box,
                                     irr_core_vector3df* p)
  {
    ((irr::core::aabbox3d<irr::f32>*)box)
      ->addInternalPoint(*(irr::core::vector3df*)p);
  }

  void
  irr_core_aabbox3d_reset(irr_core_aabbox3d_f32* box,
                          irr_core_vector3df* initValue)
  {
    ((irr::core::aabbox3d<irr::f32>*)box)
      ->reset(*(irr::core::vector3df*)initValue);
  }
}
